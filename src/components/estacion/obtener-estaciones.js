import React, { Component } from 'react';
import axios from 'axios';
import ListaEstacion from "./modificar-estacion";

class ObtenerEstaciones extends Component {
    constructor(props) {
        super(props);
        this.state = {
            estaciones: [],
        }
    }

    _GetEstaciones() {
        axios.get('http://localhost:3670/api/estaciones')
            .then(response => {
                console.log(response);
                let estacion = response.data;
                if (response.status === 200)
                    this.setState({ estaciones: estacion });
            })
            .catch(error => {
                console.log(error);
            });
    }

    componentDidMount() {
        this._GetEstaciones();
    }

    render() {
        if (this.state.estaciones === "") {
            return (<div>
                <h1>Error</h1>
            </div>)
        } else {
            return (
                <div className="container">
                    <div className="table-responsive-sm">
                        <table className="table table-bordered">
                            <caption>Lista de Estaciones</caption>
                            <thead className="thead-dark">
                                <tr>
                                    <th>Código Estación</th>
                                    <th>Nombre Estación</th>
                                    <th>Estado</th>
                                    <th>Hora Apretura</th>
                                    <th>Hora Cierre</th>
                                    <th>Código Troncal</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <ListaEstacion estaciones={this.state.estaciones} />
                            </tbody>
                        </table>
                    </div>
                </div>
            )
        }
    }
}


export default ObtenerEstaciones;