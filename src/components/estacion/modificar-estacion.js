import React, { Component } from 'react';
import axios from 'axios';
import { ListarTroncales } from "./agregar-estacion";

class ListaEstacion extends Component {
    constructor(props) {
        super(props)

        this.state = {
            estacion: {
                codEstacion: "",
                nombre: "",
                estado: "",
                horaApertura: "",
                horaCierre: "",
                codTroncal: "",
            },
            troncales: [],
            mensaje: "",
            codEstacionDelete : ""
        }

        this._GetEstacion = this._GetEstacion.bind(this);
        this._PutEstacion = this._PutEstacion.bind(this);
        this.handleOnChange = this.handleOnChange.bind(this);
        this._DeleteEstacion = this._DeleteEstacion.bind(this);
        this.handleCodigoEstacion = this.handleCodigoEstacion.bind(this);
    }

    _GetEstacion(codTroncal) {
        axios.get("http://localhost:3670/api/estacion/" + codTroncal)
            .then(response => {
                console.log(response);
                this.setState({ estacion: response.data })
            }).catch(error => {
                console.log(error.response);
            })
    }

    _GetTroncales() {
        axios.get("http://localhost:3670/api/troncales")
            .then(response => {
                this.setState({ troncales: response.data });
                console.log(response);
            }).catch(error => {
                console.log(error.response);
            });
    }

    handleOnChange(evt) {
        let estacion = this.state.estacion;
        estacion[evt.target.name] = evt.target.value;
        this.setState({ estacion: estacion })
    }

    handleCodigoEstacion(codEstacion){
        this.setState({codEstacionDelete : codEstacion});
    }

    _PutEstacion(evt) {

        evt.preventDefault();

        axios.put("http://localhost:3670/api/estacion", this.state.estacion)
            .then(response => {
                console.log(response);
                this.setState({ mensaje: response.data.message });
                window.history.go("/api/estacion");
            }).catch(error => {
                console.log(error.response);
                this.setState({ mensaje: error.response.data.message });
            });

    }

    _DeleteEstacion(){
        axios.delete("http://localhost:3670/api/estacion/" + this.state.codEstacionDelete)
        .then(response =>{
            console.log(response);
                this.setState({ mensaje: response.data.message });
                window.history.go("/api/estacion");
                alert(this.state.mensaje);
        }).catch(error => {
            console.log(error.response);
            this.setState({ mensaje: error.response });
            alert(this.state.mensaje);
        });
    }

    componentDidMount() {
        this._GetTroncales();
    }

    render() {
        return (
            this.props.estaciones.map(estacion =>
                <tr key={estacion.id}>
                    <th scope="row">{estacion.codEstacion}</th>
                    <td>{estacion.nombre}</td>
                    <td>{estacion.estado}</td>
                    <td>{estacion.horaApertura}</td>
                    <td>{estacion.horaCierre}</td>
                    <td>{estacion.codTroncal}</td>
                    <td>
                        <button className="btn btn-primary" onClick={() => this._GetEstacion(estacion.codEstacion)} data-toggle="modal" data-target="#modalModificar">Modificar</button>
                        <ModalModificar actualizar={this._PutEstacion} codEstacion={this.state.estacion.codEstacion} troncales={this.state.troncales} nombre={this.state.estacion.nombre}
                            estado={this.state.estacion.estado} horaApertura={this.state.estacion.horaApertura} horaCierre={this.state.estacion.horaCierre} codTroncal={this.state.estacion.codTroncal}
                            handleOnChange={this.handleOnChange} />
                    </td>
                    <td>
                        <button onClick={() => this.handleCodigoEstacion(estacion.codEstacion)} className="btn btn-danger" data-toggle="modal" data-target=".bd-example-modal-sm">Eliminar</button>
                        <ModalEliminar eliminar={this._DeleteEstacion}/>
                    </td>
                </tr>
            )
        );
    }
}

function ModalModificar(props) {
    return (
        <div className="modal fade" id="modalModificar" tabIndex="-1" role="dialog" aria-labelledby="modalModificar" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="modalModificar">Mensaje de la página</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <form onSubmit={props.actualizar}>
                            <div className="card-body">
                                <div className="form-row">
                                    <div className="form-group col-md-5">
                                        <label className="label">Código Estación</label>
                                        <input className="input" name="codEstacion" type="text" placeholder="Ingrese código estación" value={props.codEstacion} onChange={props.handleOnChange} disabled />
                                    </div>
                                    <div className="form-group col-md-5">
                                        <label className="label">Nombre Estación</label>
                                        <input className="input" name="nombre" type="text" placeholder="Ingrese nombre estación" value={props.nombre} onChange={props.handleOnChange} required />
                                    </div>
                                    <div className="form-group col-md-2">
                                        <label className="label">Estado</label>
                                        <select className="form-control" type="text" name="estado" value={props.estado} onChange={props.handleOnChange} required>
                                            <option>Seleccionar</option>
                                            <option>Abierta</option>
                                            <option>Cerrada</option>
                                            <option>Bloqueada</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="form-row">
                                    <div className="form-group col-md-4">
                                        <label className="label">Hora Apertura</label>
                                        <input className="input" name="horaApertura" type="text" placeholder="HH:mm:ss" value={props.horaApertura} onChange={props.handleOnChange} required />
                                    </div>
                                    <div className="form-group col-md-4">
                                        <label className="label">Nombre Cierre</label>
                                        <input className="input" name="horaCierre" type="text" placeholder="HH:mm:ss" value={props.horaCierre} onChange={props.handleOnChange} required />
                                    </div>
                                    <div className="form-group col-md-3">
                                        <label className="label">Código Troncal</label>
                                        <select className="form-control" name="codTroncal" type="text" value={props.codTroncal} onChange={props.handleOnChange} required>
                                            <option>Seleccionar</option>
                                            <ListarTroncales troncales={props.troncales} />
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" className="btn btn-primary">Modificar Estación</button>
                            </div>
                        </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

function ModalEliminar(props) {
    return (
        <div className="modal fade bd-example-modal-sm" tabIndex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div className="modal-dialog modal-sm modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">Advertencia</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <h5>¿Esta seguro que desea eliminar esta estación?</h5>
                    </div>
                    <div className="modal-footer">
                        <button onClick={props.eliminar} type="submit" className="btn btn-primary">Eliminar Estación</button>
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ListaEstacion;