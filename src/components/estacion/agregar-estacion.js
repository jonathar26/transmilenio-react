import React, { Component } from "react";
import axios from "axios";

class AgregarEstacion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            estacion: {
                codEstacion: "",
                nombre: "",
                estado: "",
                horaApertura: "",
                horaCierre: "",
                codTroncal: "",
            },
            mensaje: "",
            troncales: []
        }

        this.handleOnChange = this.handleOnChange.bind(this);
        this.submitEstacion = this.submitEstacion.bind(this);

    }

    _GetTroncales() {
        axios.get("http://localhost:3670/api/troncales")
            .then(response => {
                this.setState({ troncales: response.data });
                console.log(response);
            }).catch(error => {
                console.log(error.response);
            });
    }

    componentDidMount() {
        this._GetTroncales();
    }

    handleOnChange(evt) {
        let estacion = this.state.estacion;
        estacion[evt.target.name] = evt.target.value;
        this.setState({ estacion: estacion })
    }

    submitEstacion(evt) {

        evt.preventDefault();

        axios.post("http://localhost:3670/api/estacion", this.state.estacion)
            .then(response => {
                this.setState({ mensaje: response.data.message });
                console.log(response);
            })
            .catch(error => {
                this.setState({ mensaje: error.response.data.message });
                console.log(error.response);
            });
    }

    render() {
        return (
            <div className="container">
                <div className="card">
                    <div className="card-header">
                        <h3 className="card-title" >Adicionar una estación</h3>
                    </div>
                    <form onSubmit={this.submitEstacion}>
                        <div className="card-body">
                            <div className="form-row">
                                <div className="form-group col-md-5">
                                    <label className="label">Código Estación</label>
                                    <input className="input" name="codEstacion" type="text" placeholder="Ingrese código estación" value={this.state.estacion.codEstacion} onChange={this.handleOnChange} required />
                                </div>
                                <div className="form-group col-md-5">
                                    <label className="label">Nombre Estación</label>
                                    <input className="input" name="nombre" type="text" placeholder="Ingrese nombre estación" value={this.state.estacion.nombre} onChange={this.handleOnChange} required />
                                </div>
                                <div className="form-group col-md-2">
                                    <label className="label">Estado</label>
                                    <select className="form-control" type="text" name="estado" value={this.state.estacion.estado} onChange={this.handleOnChange} required>
                                        <option>Seleccionar</option>
                                        <option>Abierta</option>
                                        <option>Cerrada</option>
                                        <option>Bloqueada</option>
                                    </select>
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-md-5">
                                    <label className="label">Hora Apertura</label>
                                    <input className="input" name="horaApertura" type="text" placeholder="HH:mm:ss" value={this.state.estacion.horaApertura} onChange={this.handleOnChange} required />
                                </div>
                                <div className="form-group col-md-5">
                                    <label className="label">Nombre Cierre</label>
                                    <input className="input" name="horaCierre" type="text" placeholder="HH:mm:ss" value={this.state.estacion.horaCierre} onChange={this.handleOnChange} required />
                                </div>
                                <div className="form-group col-md-2">
                                    <label className="label">Código Troncal</label>
                                    <select className="form-control" name="codTroncal" type="text" value={this.state.codTroncal} onChange={this.handleOnChange} required>
                                        <option>Seleccionar</option>
                                        <ListarTroncales troncales={this.state.troncales} />
                                    </select>
                                </div>
                            </div>
                            <button type="submit" className="btn btn-primary" data-toggle="modal" data-target="#modalAgregado">Agregar Estación</button>
                        </div>
                    </form>
                </div>
                <ModalAgregado mensaje={this.state.mensaje} />
            </div>
        );
    }
}

export function ListarTroncales(props) {
    return (
        props.troncales.map(troncal =>
            <option key={troncal.id} >{troncal.codTroncal}</option>
        )
    );
}

function ModalAgregado(props) {
    return (
        <div className="modal fade" id="modalAgregado" tabIndex="-1" role="dialog" aria-labelledby="modalAgregado" aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="modalAgregado">Mensaje de la página</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        {props.mensaje}
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default AgregarEstacion;