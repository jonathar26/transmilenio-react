import './App.css';
import React, { } from 'react';
import ObtenerEstaciones from './components/estacion/obtener-estaciones'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import 'bulma/css/bulma.css'
import AgregarEstacion from './components/estacion/agregar-estacion';

function App() {
  return (
    <Router>
      <div>
        <header>
          <nav className="navbar navbar-expand-lg navbar navbar-dark bg-dark">
            <p className="navbar-brand">
              <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPgAAADLCAMAAAB04a46AAAA81BMVEUAAAD/////9wDo6OgxMTE7OzvAwMD//wD/+wD/+gD//QD/+AD/9QD39/fa2trk5OTLy8udnZ1WVQBtbW28vLy0tLOSkpI2NjaLi4t0dHRNTk9kZGSsrAAoKCokJCRJSUnGwgCHiI1UVFSlpaVeX2lISFOalAD08QDb1wCIhQBBQUEbGxsTExM7OQDl5ADy7gDNywBLSgB+fn4QDQBFQABiYAB8dQBxbAChoQC7ugDe2gA1MgAtKwCysQDIwgASEwAhHgCPjQAODiNRTwAqKjgZGAAAAAwoJQAbHDVDRFNtbnZRUloAACIbGylycgB8fABjY2xZj75dAAAMOklEQVR4nO2dCXvauBaGbUziBcwWlhAIEJiyrwkESEgLk2kzd3oH7v//NVeyZFsC0iyVyKH19zwdDDiSXi1HR0cyo6i/qZSXbtBK4SNVKf1z4MrR6jQAD8AD8AA8AP+FwK9rWaQrpOs/HH3/RHWR/zXAb24Wi954PJlMbjeber0+KxaLD/PV4n40SaUmt0iret35fOaoOC4cGfiyN05NppsWKntxXhkO+/3BoNEwLEvnZSGZ+q4sR3pfiYMGX96PUtPbFmqoeaWJEKNRiyISAMMMIUWjkdDbZEyugI3xm3vUoJj0odK86w8apmlYOuJDwv+JvJVwvyLDPzUA4Mve42TaqhfnQ4zaMHFLIsoI5hQDuqWoOUqqHwW+vH+crFezh8/Nft9hxc1qiGrSH8t6+Ms+MPjXxTi1XpG2NdFgJZ34AKycGouMehjwm2Uvta7j1h1EHMOEe3L00LyurNknVTb4cjxpzeZN1JmxJUY2+CBd+QUNlIQ88N6kVaw0SfviOQcAryu99YcqHvzL4219PhwYLjA8RbHvIhB8mdrM5nchx7cwQx82fl+WPrlWhYDfjNerhya10R9N9Qo1Fe1nwRdOIzdCnut4DNLHWZfrHeCLx82sgqYmSz8eYiKz8s3jehP403hdr9xha31syI4i5qL2ZvD7VOuh2UcT8jGM5GdkFf/2uV4A106vlMVk9fmugQw2aHv9siKNL0ww6nlwLZ39/idyRuoYGYLP9bOyVv9RXwCPp68+fVOU0Wp+h8bzRxdYlPpK7AfgWiL7CQ3px3qlj1v6qPs2L339j/oMeCzzD+rb49YcGTHrGM32j2Q2v8X3gGuJ5Pf/Kk/TGfbCzCirjy6xIFmpK3ULHPXuaukptZoPHF/b4GVa3ickcOBGwHhFIm4XgVlXxud/NR481u5in/txfbter6fT6SS1pduHoqt5xdXQURPrrt/HUV5HjUijEbEM06LCFWdatMI+cmKImr2augWeP8e68pTklX1xafYFaYHUQxqNRqnJZHp7u2m1VvXVbFacz50aonXTaJgR1HVwlVg0rhg5RHxCLzqBtpemM87Mvwj+Sn1dPt2jisHV0mrV67MHVCM4iIwrwzR0i6xpcSVICK82lO3jER8eXkaL+cX40dk4wPH0Ie4bIadTIHNjGWLqQK9/2uYCAL6rm6/3o8kUjRW8T3TXbxhkA8hwquE9prNBA23QwXd0sxhN1i2nHvrIetJqwEiv6hD67f92uI4DfEuLMaoGPC7uBg1aC8hEPjttRPqKtsN1lOCslr3UdFPnKsFEy0g8IFyHwg+0/Urgvr4ukWVYr4qf6U6NMXAGhWENFXuX6xcCZ4QmitQ0tXwaTTb1+XCU3cP1a4ITZdO1q3++//Vve0+D/9LgJN5ia/u4fwPwZxSAB+AfBL5+EJ7kUYDPdH0mOs1jAG/p0VBTdKJHAD4iSyjBOgLwIT42EfkqOFX44BsLt7i1EJwsePCbfuT3BF85DR6yngSnCx584DR4JPJFcLrQwVd0W/K3Ax+QQEm0Lzph4OAti4bGfjcHpu/GBIeiU4YNPnUPHphz0UnDBh96we+66KRBg/e8kyb6RnTaoMGLlgf+KDptyOA3DX+fR/QaBTT4xj9TFRGeOGTwpr/fdSc8ccDgI7/BTeHTOGRw37SFDOEhN8Dgy77f0/WW8OThgq91/3iDPhKePFzwit/TQ/qN8OTBgvcGPrc5EJ8+WPAW0+CGeKMOF7zJHBS2xBt1sOBj312VsUSBC15nH2+JjMVnABW8z/R0GbYNKvgj094hS3jATQELPmNsesgoSsgBKDjb00OWBNsGFHzENnioId5hhQrO9XQptg0oeJ9tcAmLcQUoeI97js8SHlrGAgle54a4MZWRB0jwO5Y71LiXkQdE8CXX0w3xgUYsiOAtHlz84T4siOBDrqdLcV9Agn/lHh+JhmS4LyDBp1xPj/TFx9uwAII/cA9pi98ZJ4IH/qXPdXUJIXVH8MAfuZ4eteQMcYDgXNAJDXHR57yo4IE3uSFuSRri8MB7kYMMcXjgG34ys3ris3AEDvwz/4sjd3JmcXjgSy4GEdLlOOoKPPCUxYOvhedABQ28zv+IkrEUngMVNPAh/4C7jK0EImDgiwHHLf5xM0/AwKdbQ1z4gUZPwMCLHHhU/Lk+T8DA+TCjWRGdvi9Y4L0G39Nl+asKNHDeXw1ZUgLLRLDA+eBLSPgTOIxggfPBF0vGvrgrUOD3vPdiTcQmzwkU+C2/JBX+6DQrUOAPXNTJqohNnRco8K0lqZwtFCpI4PxmYagh+slpTpDAt7ZQpByE8AQJnHfUZbptCizwO859aUg4x8kIErjB/uKiKS8G4QgQOL93pK9Epr0rQOB8uM2UtGfmChB4hXNfJPd0SOB9doiLf256S3DAF1b0gD0dEPiEHeIR2T0dEDh3cFe2TYcEzu0WmrI2ST2BAf/CLs3kHFjmBAb8nv2fcMhdkToCA87bNuG/jLAjMOCs3yZzI8EVGPA5+/CwzCgjFRhw5gchQlIeQtkSFPAnZn9Ywg8j7AoK+IiZxXWpwTYqKOBMvE2+u4oFBZwx6pJO5m8JCviDDy51A8UTFPChF4WQulXoCwj4jf8UrfSVOBEQ8PuBO41b8tcnjoCAj7yefoD1iSMg4BPXYTVlnoJgBQTc2xmXHmR0BQTc/cHd0EDieR9OQMDdgJsl7Zj2toCAu2chIoeZyxQw4HMCbsh69GZXQMCHTlePGgdrcFjgRkVUei8LEnhUl3sWgBMkcHlP3uwREHD8O4XRkKjUXiMg4EUDdXRpTxztExDwjR6V9hTpfgEB7zX0Ay1HXQEBV6YrWc9SPiMo4AdXAB6AB+ABeAAegAfgH1389ysAD8AD8AA8AA/AA/CPLv77FYD/BHj1jCgfxu86znUhzGRSyl9614UCeW1X/Y9q6Wzbv7vg3X2Sv3A/rHpXSrvAJB3Ok7xJal36qlRPyOtFlblXKSdPs265RICn3bd5Jy9ybZ/7GWbVrHdtqwQhe+p+klbjCc2+9u6IqTWvcGn3w0TC+77G9rM8zTpGqkG1c+QvaYbpBHPvuZNRVRx4rlxux2vt8kWHgJ+Uy+VqWvUrO8EUW4uf8uDXKmZOqmUPPBEnF133XpyEd6Uk4yy43Ua5lS9Ij8nFNVJpaVrtGQa8oCZxtak5YeBOaZPu1YVKP/Faq6R1VB+8ShA98HiGFMQrYqyqlkhJE7VXgDNvlJNE3i49Bx4n17QyRYHHd8CvvS+rccVvfpvWiAdunzkvbW9oxKoxcp3JZt4InouVtOwz4AVahrwalgzuFalWUxLusFXsTsFuc+D+6HbBk6S4sXb6zeDdc/sZcM80EFMkD7zmlS92ppzHPPBLJZZhwU9tdgLAtxdyKrYWZVt5O3ipq53vB/dSIGZAGnjYdj8JxztK20Gh4GfY9HrgYVU9vc5x4IqNZ6xs5nXgXSQfXMna+8E1tzynp7LAk0g1O+GW5gwXVHUnXwSuxGvsdFaNoZRi/uSMLh3gxNlrwFUbq+CDl/DY2QfuTqnkM/HgZTWOpGa8r2r4q4T7HoNf210GHNVVLa6q3lBH4GdaV8mhf69p8XAbqeuDO4P5R+BpSeCkq9f8HNEQ7/ilxeBdVAgWHKmgeYMVNz7ycvIohXeMcUW5VM/2gXslTKRlgldt6jQqYS0ei8U06jc44AoyvVvgStUbDBg8kVROz98JrmRi+8C9tOysTHBFc+flfAJ7cm0yiVDwjnZ9TsvRoTa9q+UZcDQLxNrvBS+p7cwueFYj4+GE1LA08Iw7gWWuuZI74EpSK9D3ZzSNDnVkCHhOy+O/fx84stvJXfA27VJJrSMVvKDSvh674CqcgHfUmGvt1DN6vzvhOQY+5jjW7wTPqdoelzVBmoKaXWngXZr3SYwAX9hlBhytFVzwvIpvrNoemQOedRzM/eBa1RH96wvnTZkFVxLuHMGCXzhZxmkFCwP3fNIq9dzSpGHO3ayJTVFUAt7157tzlI6tMuYXd4FqHNeXv3Jh1ndJmnmHgLMlycVLlJG2OLcsRffGVJuu/EWB50ruVZd28Q7J4dL9IkysmOuj5fzYxOV1JuvHGZSc00Wcuy89bzbs3146IaK5kDfkxq6b+gkdN5ecOxw+z1y7blUQegrAA/AAPAAPwD3wy5OjUs5T6Yfg/weDk2e1pYfK/gAAAABJRU5ErkJggg==" width="60" height="50" alt="" className="d-inline-block align-top" />
              Transmilenio
            </p>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavDropdown">
              <ul className="navbar-nav">
                <li className="nav-item dropdown">
                  <h6 className="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Estaciones
                    </h6>
                  <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <Link className="dropdown-item" to={"/api/estacion"}>Agregar Estación</Link>
                    <Link className="dropdown-item" to={"/api/estaciones"}>Consultar todas las estaciones</Link>
                    <Link className="dropdown-item" to={"/api/consultar/eliminar/estacion"}>Consultar o borrar estación</Link>
                  </div>
                </li>
                <li className="nav-item dropdown">
                  <h6 className="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Rutas
                    </h6>
                  <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <Link className="dropdown-item" to={"/api/ruta"}>Agregar Ruta</Link>
                    <Link className="dropdown-item" to={"#"}>Consultar todas las rutas</Link>
                    <Link className="dropdown-item" to={"#"}>Consultar ruta por Id</Link>
                  </div>
                </li>
                <li className="nav-item dropdown">
                  <h6 className="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Troncales
                  </h6>
                  <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <Link className="dropdown-item" to={"/api/troncal"}>Agregar Troncal</Link>
                    <Link className="dropdown-item" to={""}>Consultar todas las troncales</Link>
                  </div>
                </li>
              </ul>
            </div>
          </nav>
        </header>
      </div>
      <div>
        <div>
          <Switch>
            <Route path="/api/estaciones" component={ObtenerEstaciones}></Route>
            <Route path="/api/estacion" component={AgregarEstacion}></Route>
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
